﻿/*
Savagedlight.Network.Client
Copyright (c) 2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using Savagedlight;
using Savagedlight.Serialization;
using Savagedlight.Network;
using Savagedlight.Network.Exceptions;
using Savagedlight.Network.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using Savagedlight.Network.Packets;

namespace Savagedlight.Network.Client
{
    public sealed class NetworkClient
    {
        private TcpClient client;
        private NetworkStream Stream { get; set; }
        
        private Queue<NetworkSendQueueEntry> sendQueue = new Queue<NetworkSendQueueEntry>();
        private AutoResetEvent sendQueueAre = new AutoResetEvent(false);

        private Queue<ClientEventArgs> eventQueue = new Queue<ClientEventArgs>();
        private AutoResetEvent eventQueueAre = new AutoResetEvent(false);

        private PacketHandler packetHandler { get; set; }
        private Thread sendThread;
        private Thread receiveThread;
        private Thread eventDispatcherThread;

        private System.Diagnostics.Stopwatch timeSinceLastIncomingPacket;

        public byte[] Handshake { set; private get; }

        #region Events
        public event ClientEventHandler<DataReceivedEventArgs> DataReceivedEvent;
        #endregion


        #region Constructors
        private NetworkClient()
        {
            this.client = new TcpClient();
            // Don't wait for buffers to fill up
            this.client.NoDelay = true;
            this.Handshake = new byte[] { 1, 3, 5, 7 };
        }

        public NetworkClient(PacketHandler packetHandler)
            :this()
        {
            this.packetHandler = packetHandler;            
        }

        public NetworkClient(params Assembly[] assemblies)
            :this()
        {
            this.packetHandler = new PacketHandler(assemblies);
        }
        #endregion
        
        public void Connect(string hostname, int port)
        {
            this.client.Connect(hostname, port);
            this.Stream = this.client.GetStream();
            this.StartThreads();
        }

        public void Connect(IPAddress ip, int port)
        {
            this.client.Connect(ip, port);
            this.Stream = this.client.GetStream();
            this.StartThreads();
        }
        
        /// <summary>
        /// Queues a packet for sending to server.
        /// </summary>
        /// <param name="packet"></param>
        public void Write(Packet packet)
        {
            byte[] toWrite = this.packetHandler.SerializePacket(packet);
            this.sendQueue.Enqueue(new NetworkSendQueueEntry(packet.Id, toWrite));
            this.sendQueueAre.Set();
        }

        #region Threaded methods
        private void PacketReader()
        {
            byte[] buffer;
            int pos;
            try
            {
                while (true)
                {
                    while (!this.Stream.DataAvailable && this.client.Connected)
                    {
                        Thread.Sleep(1);
                    }
                    if (!this.client.Connected)
                    {
                        Console.WriteLine("Reader: Disconnected");
                        break;
                    }

                    buffer = new byte[4];
                    pos = 0;
                    while (pos < buffer.Length)
                    {
                        pos += this.Stream.Read(buffer, pos, buffer.Length - pos);
                        if (!this.client.Connected) { throw new InvalidOperationException(); }
                    }
       
                    // We should now have the length and packet ID.
                    ushort length = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(buffer, 0));
                    ushort packetId = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(buffer, 2));

                    if (!this.packetHandler.IsValidPacketId(packetId))
                    {
                        throw new InvalidPacketIdException(packetId);
                    }

                    if (!this.packetHandler.IsValidPacketDirection(packetId, PacketDirection.ServerClient))
                    {
                        throw new InvalidDirectionPacketIdException(packetId, PacketDirection.ServerClient);
                    }


                    // Read the payload.
                    buffer = new byte[length];
                    pos = 0;
                    while (pos < buffer.Length)
                    {
                        pos += this.Stream.Read(buffer, pos, buffer.Length - pos);
                        if (!this.client.Connected) { throw new InvalidOperationException(); }
                    }

                    // Restart the stopwatch.
                    this.timeSinceLastIncomingPacket.Reset();
                    this.timeSinceLastIncomingPacket.Start();

                    // Process the payload
                    Packet packet = this.packetHandler.DeserializePacket(packetId, buffer);
                    if (packet.Id <= Packet.HighestReservedPacketId)
                    {
                        this.HandleReservedPacket(packet);
                    }
                    else
                    {

                    }
                    Console.WriteLine("Received packet: {0} ({1})", packet.Id, packet.GetType().Name);
                }
            }
            catch (InvalidPacketIdException ex)
            {
                Console.WriteLine("Invalid packet ID: {0}", ex.PacketId);
            }
            catch (InvalidDirectionPacketIdException ex)
            {
                Console.WriteLine("Invalid packet direction: " + ex.ToString());
            }
            catch (InvalidOperationException)
            {
            }
            finally
            {
                if (this.client.Connected)
                {
                    this.client.Close();
                }
                Console.WriteLine("Connection closed.");
            }
        }

        private void HandleReservedPacket(Packet packet)
        {
            switch (packet.Id)
            {
                case Packet.PingPacket:
                    this.Write(new PongPacket());
                    Console.WriteLine("Ping? Pong!");
                    break;
                case Packet.PongPacket:
                    this.expectingPongPacket = false;
                    Console.WriteLine("Pong!");
                    break;
            }
        }

        private bool expectingPongPacket = false;

        private void PacketSender()
        {
            // Send the handshake.
            this.Stream.Write(this.Handshake, 0, this.Handshake.Length);

            using (NetworkStream stream = this.client.GetStream())
            {
                try
                {
                    while (this.client.Connected)
                    {

                        while (!this.sendQueueAre.WaitOne(2000))
                        {
                            if (this.timeSinceLastIncomingPacket.ElapsedMilliseconds > 2000)
                            {
                                if (!this.expectingPongPacket)
                                {
                                    this.expectingPongPacket = true;
                                    this.Write(new PingPacket());
                                    Console.WriteLine("Ping?");
                                    this.timeSinceLastIncomingPacket.Reset();
                                    this.timeSinceLastIncomingPacket.Start();
                                    continue;
                                }
                                this.Write(new DisconnectPacket("Ping Timeout"));
                            }
                        }
                        // Lazy check
                        while (this.sendQueue.Count > 0)
                        {
                            lock (this.sendQueue)
                            {
                                // Verify the lazy check
                                if (this.sendQueue.Count == 0) { break; }
                                NetworkSendQueueEntry nsqe = this.sendQueue.Dequeue();
                                nsqe.Write(this.Stream);
                                if (nsqe.PacketId == Packet.DisconnectPacket)
                                {
                                    return;
                                }

                                if (this.sendQueue.Count == 0)
                                {
                                    Thread.Sleep(2);
                                }
                            }
                        }

                    }
                }
                catch (IOException)
                {
                }
                finally
                {
                    if (this.client.Connected)
                    {
                        this.client.Close();
                    }
                    Console.WriteLine("WriterThread: Connection closed.");
                }
            }
        }

        private void EventDispatcher()
        {
            while (true)
            {
                try
                {
                    this.eventQueueAre.WaitOne();
                    // Lazy check
                    while (this.eventQueue.Count > 0)
                    {
                        lock (this.eventQueue)
                        {
                            // Verify the lazy check
                            if (this.eventQueue.Count == 0) { break; }

                            ClientEventArgs e = this.eventQueue.Dequeue();
                            this.ProcessEvent(e);

                            if (this.sendQueue.Count == 0)
                            {
                                Thread.Sleep(2);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error sending packet: " + ex.ToString());
                    this.client.Close();
                }
            }
        }
        #endregion


        #region Helper methods
        private void StartThreads()
        {
            this.timeSinceLastIncomingPacket = System.Diagnostics.Stopwatch.StartNew();
            sendThread = new Thread(new ThreadStart(this.PacketSender));
            sendThread.IsBackground = true;
            sendThread.Name = "Client: SendThread";
            sendThread.Priority = ThreadPriority.BelowNormal;
            sendThread.Start();

            receiveThread = new Thread(new ThreadStart(this.PacketReader));
            receiveThread.IsBackground = true;
            receiveThread.Name = "Client: ReceiveThread";
            receiveThread.Priority = ThreadPriority.BelowNormal;
            receiveThread.Start();

            eventDispatcherThread = new Thread(new ThreadStart(this.EventDispatcher));
            eventDispatcherThread.IsBackground = true;
            eventDispatcherThread.Name = "Client: EventDispatcherThread";
            eventDispatcherThread.Priority = ThreadPriority.BelowNormal;
            eventDispatcherThread.Start();
        }

        private void ProcessEvent(ClientEventArgs e)
        {
            switch (e.Type)
            {
                case ClientEventType.DataReceived:
                    this.DispatchEvent<DataReceivedEventArgs>(
                        this.DataReceivedEvent, 
                        (DataReceivedEventArgs)e);
                    break;
                case ClientEventType.ConnectionState:
                    break;
            }
        }

        private void DispatchEvent<T>(ClientEventHandler<T> evnt, T e)
             where T : ClientEventArgs
        {
            if (evnt == null) { return; }
            lock (evnt)
            {
                evnt(this, e);
            }
        }
        #endregion
    }
}
